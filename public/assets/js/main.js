/**
 * Created by dmitriysinkov on 03.05.14.
 */
$(function(){
    $('#add_param_button').click(function(e){
        e.preventDefault();
        var algorithm_id = $('#add_param_button').attr('algorithm_id');
        var post_url = $('#add_param_button').attr('href');
        $.post(post_url,
            {
              'algorithm_id' : algorithm_id,
            },
            function(data){
                $('#accordion').append(data);
                $('.type_param').change();

            }
        );
    });


    $(document).on("click", '#usebtn', function(e){
        var str = $("#fuzzyvars").serialize();
        console.log(str);
        $.post($(this).attr('href'),str,function(data){
            $('#result_content').prepend(data);
        });
    });


    $( document ).on("click", '.add_list', function(e){
        e.preventDefault();
        var algorithm_id = $('#add_param_button').attr('algorithm_id');
        var param_id = $(this).attr('param_id');
        var post_url = $(this).attr('href');
        $.post(post_url,
            {
                'algorithm_id' : algorithm_id,
                'param_id': param_id
            },
            function(data){
                $('#list_values_'+param_id).append(data);
                generate_options(param_id);
            }
        );
    });

    $( document ).on("click", '.del_list_val', function(e){
        e.preventDefault();
        var algorithm_id = $('#add_param_button').attr('algorithm_id');
        var list_id = $(this).attr('list_id');
        var param_id = $(this).closest('.params_block').attr('pid');
        var post_url = $(this).attr('href');
        var obj = this;
        $.post(post_url,
            {
                'algorithm_id' : algorithm_id,
                'param_id':param_id,
                'list_id': list_id
            },
            function(data){
                if(data.error == 0)
                    $(obj).closest('.keys_block').remove();

                generate_options(param_id);
            }
        );
    });

    function generate_options(param_id){
        console.log(param_id);
        var objs = $('#list_values_'+param_id).find('.keys_block');
        var list = $('#default2_'+param_id).html('');
        for(var i=0;i<objs.length;i++)
        {
            var o = $('<option/>',
            {
                'text': $(objs[i]).find('input._value').val(),
                'value': $(objs[i]).find('input._key').val()
            }
            );
            list.append(o);
        }
    }


    $( document ).on('keypress','._key', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('keyup','._key', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('keydown','._key', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('blur','._key', function(e){

        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);

    });
    $( document ).on('change','._key', function(e){
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });


    $( document ).on('keypress','._value', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('keyup','._value', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('keydown','._value', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });
    $( document ).on('blur','._value', function(e){

        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);

    });
    $( document ).on('change','._value', function(e){
        var param_id = $(this).closest('.params_block').attr('pid');
        generate_options(param_id);
    });


    $( document ).on('change','.type_param', function(e){
        var vals = $(this).val();
        var prm_id = $(this).closest('.item_prm').attr('param_id');
        $('.tiles'+'_'+prm_id).css('display','none');
        $('#param_type_'+vals+'_'+prm_id).css('display','block');
    });


    $( document ).on('keypress','.prm_name', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).attr('param_id');
        $('#caption_'+param_id).html($(this).val());
    });
    $( document ).on('keyup','.prm_name', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).attr('param_id');
        $('#caption_'+param_id).html($(this).val());
    });
    $( document ).on('keydown','.prm_name', function(e){
        if(e.keyCode == 13)
            e.preventDefault();
        var param_id = $(this).attr('param_id');
        $('#caption_'+param_id).html($(this).val());
    });
    $( document ).on('blur','.prm_name', function(e){

        var param_id = $(this).attr('param_id');
        if(!$(this).val())
        {
            $(this).val("Новый параметр");
            $(this).change();
        }
    });
    $( document ).on('change','.prm_name', function(e){
        var param_id = $(this).attr('param_id');
        $('#caption_'+param_id).html($(this).val());
    });

    $( document ).on('click','.nav-tabs li', function(e){
        var vl = $(this).find('a').attr('href');
        $.cookie('alg_tab', vl);
    });

    $( document ).on('click','#save_param_button', function(e){
        e.preventDefault();

        var algorithm_id = $('#add_param_button').attr('algorithm_id');
        var param_id = $(this).closest('.item_prm').attr('param_id');
        var post_url = $(this).attr('href');
        var obj = $(this);
        $.post(post_url,
            {
                'algorithm_id' : algorithm_id,
                'param_id': param_id,
                'data':save_param_value()
            },
            function(data){
                console.log(data);
            }
        );
    });

    $( document ).on('click','.del_param_bnt', function(e){
        e.preventDefault();

        var algorithm_id = $('#add_param_button').attr('algorithm_id');
        var param_id = $(this).closest('.item_prm').attr('param_id');
        var post_url = $(this).attr('href');
        var obj = $(this);
        $.post(post_url,
            {
                'algorithm_id' : algorithm_id,
                'param_id': param_id
            },
            function(data){
                obj.closest('.panel').remove();
            }
        );

    });
    $(document).on('click','.pr_btn', function(e){
        e.preventDefault();
        location.href = $(this).attr('href');
    });



    after();
});

function after(){
    $('.type_param').change();
    var val_tab = $.cookie('alg_tab');
    $('a[href=' + val_tab + ']').click();

}

function save_param_value(){
    var post_obj = new Object();
    var params_div = $('.item_prm');

    params_div.each(function(idx) {
        //console.log($(this).attr('param_id'));
        var p_id = $(this).attr('param_id');
        post_obj[p_id] = new Object();
        $(this).find('input, select').each(function(i){

            var name = $(this).attr('name');
            var val = $(this).val();

            if($(this).attr('type') == 'checkbox')
                val = $(this).is(":checked") ? 1 : 0;

            post_obj[p_id][name] = val;

        });

    });
    console.log(post_obj);
    return post_obj;
}