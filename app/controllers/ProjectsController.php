<?php
use FuzzySystem\FuzzySystem;
class ProjectsController extends BaseController{
	
	public function getIndex(){

        $projects = Sentry::getUser()->projects;
        $projects_ = array();
        if($projects)
            foreach($projects as $project){
                $projects_[$project->state][] = $project;
            }
        return View::make('projects.getindex')->with(
            array(
                'projects'=>$projects_,
                'pojectStatuses'=>$this->pojectStatuses
            )
        );
	}

    function is_dominant($coord, $data, $type){

        foreach($data as $item)
        {
            if($coord[0] >= $item[0]){
                if($type != 2){
                    if($coord[1]>$item[1]){
                        return false;
                    }
                }else{
                    if($coord[1]<$item[1]){
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function getPareto($data, $project){
        $pareto = array();
        $pareto_buf = $data;
        $exit = true;
        if(!$data)
            return array();
        $k =0;
        while($exit){
            $exit = false;
            //print_r('<pre>');
            foreach($pareto_buf as $coord){
                //print_r('<br>>>>1');
                //print_r($coord);
                //print_r('<br>>>>2');
                //print_r($pareto_buf);
                //var_dump($this->is_dominant($coord,$pareto_buf));
                    if($this->is_dominant($coord,$pareto_buf,$project->projecttype_id)){
                        array_push($pareto,$coord);
                    }else{
                        $exit = true;
                    }
                }
            if($exit){
                $pareto_buf = $pareto;
                //echo '<pre>';
                //print_r($pareto_buf);
                //die('2');
                $pareto = array();
            }
            $k++;
        }
        //echo '<pre>';
        //print_r($pareto);
        //die;
        return $pareto;
    }

    public function getEdit($id = 0){

        $project = Project::find($id);
        if(!$project){
            $project = new Project();
            $project->name = "Новый проект";
            $project->user_id = Sentry::getUser()->id;
        }
        $slognost = json_decode($project->slognost);
        $selected_slognost = json_decode($project->selected_slognost);

        $sel_vl = array();

        $items = $project->selectedoptions;
        if($items)
            foreach($items as $item){
                $sel_vl[$item->project_id][$item->algorithmproject_id][$item->algorithmoption_id] = $item;
            }


        $files = $project->ufsfiles;
        $ufs_plot = array();
        foreach($files as $file){
            $ufs_plot[1][] = array(doubleval($file->tra_slognost), doubleval($file->tra_tochnost));
            $ufs_plot[2][] = array($file->tst_slognost,$file->tst_tochnost);
        }

        $ufs_plot[3] = isset($ufs_plot[1]) ? $this->getPareto($ufs_plot[1],$project) : array();
        $ufs_plot[4] = isset($ufs_plot[2]) ? $this->getPareto($ufs_plot[2],$project) : array();
        //var_dump($ufs_plot[1]);die;
        $algs = Algorithm::all();
        $algorithms = array(0=>'Не выбран');
        foreach($algs as $alg){
            $algorithms[$alg->id] = $alg->name;
        }

        $alg_project = $project->algorithmprojects()->orderBy('sort')->get();

        switch($project->projecttype_id){
            case 0:
            case 1:
            case 2:
            case 3:
                $template_id = 0;
                break;
            case 4:
                $template_id = 4;
                break;
        }

        return View::make('projects.getedit')->with(
            array(
                'project' => $project,
                'slognost' => $slognost,
                'selected_slognost' => $selected_slognost,
                'algorithms' => $algorithms,
                'alg_project' => $alg_project,
                'sel_vl'=>$sel_vl,
                'plot_point' => $ufs_plot ? json_encode($ufs_plot):'null',
                'template_id'=>$template_id
            )
        );
    }

    public function postAddAlg($id = 0){
        $project = Project::find($id);
        $alg = Algorithm::find(Input::get('algorithm_id'));


        if($alg){
            $item = new AlgorithmProject();
            $item->project_id = $id;
            $item->algorithm_id = Input::get('algorithm_id');
            $item->sort = time();
            $item->save();

            $options = $alg->algorithmoptions;

            if($options){
                foreach($options as $option){
                    $soptions = new Selectedoption();
                    $soptions->project_id = $id;
                    $soptions->algorithmoption_id = $option->id;
                    $soptions->algorithmproject_id = $item->id;
                    $soptions->double_value = $option->default_double;
                    $soptions->list_value = $option->default_list;
                    $soptions->string_value = $option->default_string;
                    $soptions->bool_value = $option->default_bool;
                    $soptions->save();
                }
            }
        }

        return Redirect::route('projects.getedit', array('id'=>$project->id));
    }
    public function postDelAlg($id = 0, $itemid = 0){
        $item = AlgorithmProject::find($itemid);
        if($item){
            $vls = Selectedoption::whereRaw('project_id = ? and algorithmproject_id = ?', array($id,$itemid ))->get();
            foreach($vls as $vl){
                $vl->delete();
            }
            $item->delete();
        }

        return Redirect::route('projects.getedit', array('id'=>$id));
    }
    public function postParamOptiomizations($id = 0){
        $project = Project::find($id);
        if($project){
            $project->cycles = Input::get('cycles');
            $project->save();
        }
        return Redirect::route('projects.getedit', array('id'=>$id));
    }
    public function postParamFunctiontype($id = 0){
        $project = Project::find($id);
        if($project){
            $project->functionType = Input::get('functionType');
            $project->save();
        }
        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public  function postSaveAlgParams($id, $itemid){
        $project = Project::find($id);
        $item = AlgorithmProject::find($itemid);
        //echo '<pre>';

        //var_dump($item->algorithm->algorithmoptions);

        $algoptions = array();
        $buf = $item->algorithm->algorithmoptions;
        if($buf)
            foreach($buf as $option)
            {
                $algoptions[$option->id] = $option;
            }

       //var_dump(Input::all());die;

        $inputs = Input::all();
        $vals = array();
        foreach($inputs as $key=>$value){
            $buf = explode('_',$key);
            if(isset($buf[1]))
                $vals[$buf[1]] = $value;
        }
        if($vals){
            foreach($algoptions as $key => $value){

                    $option = $value;
                    $value_type = '';

                    if($option){
                        switch($option->type){
                            case 0:
                                $value_type = 'double_value';
                                $set_value = $vals[$key];
                                break;
                            case 1:
                                $value_type = 'bool_value';
                                $set_value = isset($vals[$key])? 1 : 0;
                                break;
                            case 2:
                                $value_type = 'list_value';
                                $set_value = $vals[$key];
                                break;
                            case 3:
                                $value_type = 'string_value';
                                $set_value = $vals[$key];
                                break;
                        }
                    }
                    $vl = Selectedoption::whereRaw('project_id = ? and algorithmproject_id = ? and algorithmoption_id = ?', array($id,$itemid, $key ))->update(array($value_type=>$set_value));

            }

        }

        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public function postResortAlg($id = 0, $item_id = 0, $type='up'){
        $project = Project::find($id);
        $alg_project = $project->algorithmprojects()->orderBy('sort')->get();
        if($alg_project){
            for($i=0;$i< count($alg_project);$i++)
            {
                if($alg_project[$i]->id == $item_id)
                {
                    if($type == 'up')
                    {
                        if(isset($alg_project[$i-1]))
                        {
                            $old_sort = $alg_project[$i]->sort;
                            $alg_project[$i]->sort = $alg_project[$i-1]->sort;
                            $alg_project[$i-1]->sort = $old_sort;
                            $alg_project[$i]->save();
                            $alg_project[$i-1]->save();
                        }
                    }else{
                        if(isset($alg_project[$i+1]))
                        {
                            $old_sort = $alg_project[$i]->sort;
                            $alg_project[$i]->sort = $alg_project[$i+1]->sort;
                            $alg_project[$i+1]->sort = $old_sort;
                            $alg_project[$i]->save();
                            $alg_project[$i+1]->save();
                        }
                    }
                }
            }
        }
        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public function  postEdit($id = 0){

        $rules = array(
            'name'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails() && $id) {
            return Redirect::route('projects.getedit', array('id'=>$id))
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {
            if(!$id){
                $project = new Project();
                $project->name="Новый проект";
                $project->projecttype_id = Input::get('projecttype_id');
                $project->user_id = Sentry::getUser()->id;
            }
            else{
                $project = Project::find($id);
                $project->name = Input::get('name');
                $project->description = Input::get('description');
            }
            $project->save();
            // redirect
            //Session::flash('message', 'Алгоритм добавлен!');
            return Redirect::route('projects.getedit', array('id'=>$project->id))
                ->withInput(Input::all());
        }
    }

    public function getDelete($id = 0)
    {
        $project = Project::find($id);

        if($project)
            $project->delete();

        return Redirect::route("projects.getindex");
    }

    public function getSetstate($id = 0, $state = 0){
        $project = Project::find($id);
        if($project){
            $project->state = $state;
            $project->save();
        }
        return Redirect::route("projects.getindex");
    }

    public function postLoadTables($id){

        $destinationPath = public_path().'/tables/';
        if(Input::hasFile('tra_data')){
            $file            = Input::file('tra_data');
            $filename        = time().'_tra_table.'.$file->getClientOriginalExtension();
            $uploadSuccess   = $file->move($destinationPath, $filename);
            if($uploadSuccess){
                $project = Project::find($id);
                if($project->table_tra)
                {
                    File::delete($destinationPath.$project->table_tra);
                }
                $project->table_tra = $filename;
                $project->save();
                $this->readTable($id);
            }
        }
        if(Input::hasFile('tst_data')){
            $file            = Input::file('tst_data');
            $filename        = time().'_tst_table.'.$file->getClientOriginalExtension();
            $uploadSuccess   = $file->move($destinationPath, $filename);
            if($uploadSuccess){
                $project = Project::find($id);
                if($project->table_tst)
                {
                    File::delete($destinationPath.$project->table_tst);
                }
                $project->table_tst = $filename;
                $project->save();
            }
        }


        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public  function postSelectSlognost($id){

        $slognost = Input::get('slognost');
        $project = Project::find($id);
        if($project){
            if($slognost){
                $project->selected_slognost = json_encode($slognost);
                $project->save();
            }
        }


        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public function getDelTables($id,$type){

        $destinationPath = public_path().'/tables/';

        $project = Project::find($id);
        if($type == 'tra')
            if($project->table_tra)
            {
                File::delete($destinationPath.$project->table_tra);
                $project->table_tra = '';
                $project->selected_slognost = '';
                $project->slognost = '';
            }
        if($type == 'tst')
            if($project->table_tst)
            {
                File::delete($destinationPath.$project->table_tst);
                $project->table_tst = '';

            }
        $project->save();
        return Redirect::route('projects.getedit', array('id'=>$id));
    }

    public function readTable($project_id){
        $project = Project::find($project_id);
        $inputs = array();
        $total_rows = 0;
        if($project->table_tra){
            $file = fopen(public_path() . '/tables/' . $project->table_tra, 'r');
            while(true){
                $str = fgets($file);
                if($str === FALSE)
                    break;
                if(strpos($str,"@inputs") !== false)
                {
                    $str = str_replace("@inputs ", "", $str);
                    $inputs = explode(",", $str);
                    foreach($inputs as $k=>$v)
                    {
                        $inputs[$k] = trim($v);
                    }

                }
                if(strpos($str,"@data") !== false){
                    while(true){
                        $str = fgets($file);
                        if($str === FALSE)
                            break;
                        $total_rows++;
                    }
                }
            }
            fclose($file);

            if($inputs){
                $sl = $this->get_struct(count($inputs), $total_rows);
                $project->slognost  =  json_encode($sl);
            }
            if($total_rows){
                $project->total_rows_tra = $total_rows;
            }
            $project->save();


        }
    }

    function get_struct($count_inputs, $total_rows)
    {
        //ini_set('memory_limit', '256M');
        $structures = array();
        $slognost = array();
        $max_len = 9;
        $array = array_fill(0, $count_inputs, 2);
        $k=0;

        while($array[0]< 10)
        {
            $buff = $array;
            sort($buff);
            $key = implode(',',$buff);
            if(!isset($structures[$key])){
                $structures[$key] = $array;
                $pr = 1;
                $sum = 0;
                foreach($array as $elem)
                {
                    $pr *= $elem;
                    $sum += $elem;
                }
                $sl = $pr + $sum;
                if($sl <= $total_rows)
                    $slognost[$sl][] = $key;
            }

            $array[$count_inputs - 1]++;
            for($j=1; $j < $count_inputs; $j++)
            {
                for($i=1; $i < $count_inputs; $i++)
                {
                    if($array[$i] > 9)
                    {
                        $array[$i - 1]++;
                        $array[$i] = 2;
                    }else
                    {
                        $k++;
                    }
                }
                if($k == $count_inputs-1)
                    break;
            }
        }

        ksort($slognost,SORT_NUMERIC);
        return $slognost;

    }

    public function use_system($ufs_file_id = 0){

        $file = Ufsfile::find($ufs_file_id);

        $system = new FuzzySystem();
//
        $path = public_path().'/systems_ufs/'.$file->project->id.'/'.$file->file;
        $system->read_from_xml($path);

        //var_dump($system->variables);die;

        return View::make('projects.usesystem')->with(
            array(
                'ufsfile'=>$file,
                'fuzzy_systems' => $system
            )
        );
    }
    public function get_system_val($ufs_file_id = 0){

        $file = Ufsfile::find($ufs_file_id);

        $vars_value = Input::all();
        $system = new FuzzySystem();
//
        $path = public_path().'/systems_ufs/'.$file->project->id.'/'.$file->file;
        $system->read_from_xml($path);

        $rez = $system->inference($vars_value);

        return View::make('projects.partials.result_alert')->with(
            array(
                'result'=>$rez,
                'fuzzy_systems' => $system,
                'vars_value' => $vars_value
            )
        );
    }

}