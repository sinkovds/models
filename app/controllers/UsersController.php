<?php 

class UsersController extends BaseController{
	public function getLogin(){

    	$this->layout = 'layout.login';
        return View::make('users.login');
    }
	public function postLogin(){

        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        try
        {
            $user = Sentry::authenticate($credentials, false);

            if ($user)
            {
                return Redirect::route('app.getindex');
            }
        }
        catch(\Exception $e)
        {
            return Redirect::route('users.login')->withErrors(array('login' => $e->getMessage()));
        }
    }


    public function getLogout(){

        Sentry::logout();
        return Redirect::route('users.login');
    }

    public function getUsersgroupIndex(){

    	return View::make('users.usersgroup_index');
    }

    public function getUsersgroupEdit($id = 0){

    	return View::make('users.usersgroup_edit');
    }

    public function getUsersgroupDelete($id = 0){

    	return Redirect::route('users.usersgroup.index');
    }

    public function getUsersIndex(){

        $view = View::make('users.users_index');
        return $view;
    }

    public function getUsersEdit($id = 0){

    	return View::make('users.users_edit');
    }

    public function getUsersDelete($id = 0){

    	return Redirect::route('users.users.index');
    }

    public function getRegister(){
        $this->layout = 'layout.login';
        return View::make('users.users_register');
    }
    public function postRegister(){

        try
        {
        $user = Sentry::createUser(array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password'),
            'first_name'=> Input::get('first_name'),
            'last_name'=> Input::get('last_name'),
            'activated' => true,
        ));
        }catch(\Exception $e)
        {
             return Redirect::route('users.register')->withErrors(array('login' => $e->getMessage()));
        }
        return View::make('users.users_register_success');
    }
}