<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

    public $upload_dir = "/systems_ufs/";

    public $algorithmTypes = array(
        0=>'Генерация правил',
        1=>'Аппроксимация',
        2=>'Обучение классификаторов'
    );

    public $projectTypes = array(
        0=>'Аппроксимация (Двухкритериальная оптимизация)',
        1=>'Аппроксимация (Трехкритериальная оптимизация)',
        2=>'Классификация (Двухкритериальная оптимизация)',
        3=>'Классификация (Трехкритериальная оптимизация)',
        4=>'Ассоциативные правила'
    );

    public $optionTypes = array(
        0=>'Число',
        1=>'Бинарное',
        2=>'Список',
        3=>'Строка'
    );

    public $pojectStatuses = array(
        0=>'На редактировании',
        1=>'Готов к оптимизации',
        -1=>'В работе',
        2=>'Оптимизация завершена',
        99=>'Ошибка оптимизации'
    );

    public $functionTypes = array(
        'Triangle'=>'Треугольник',
        'Gauss'=>'Гауссоида',
        'Parabola'=>'Парабола',
        'Trapezium'=>'Трапеция'
    );

	protected function setupLayout()
	{

		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    public function __construct()
    {
        // Fetch the User object
        $user = Sentry::getUser();

        View::share('user', $user);
        View::share('projectTypes', $this->projectTypes);
        View::share('functionTypes', $this->functionTypes);
    }

}

