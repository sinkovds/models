<?php


class AlgorithmsController extends BaseController{
	
	public function getIndex(){

        $algorithms = array();
        $algorithms_ = Algorithm::all();
        if($algorithms_)
            foreach($algorithms_ as $algorithm)
            {
                $algorithms[$algorithm->type][] = $algorithm;

            }

        return View::make('algorithms.getindex')->with(array('algorithms'=>$algorithms));
	}

    public  function  getEdit($id = 0){

        $algorithm = Algorithm::findOrNew($id);
        $options = $algorithm->algorithmoptions;
        //var_dump($options[0]);
        //die('count'.count($options));
        $lists_select = array();
        foreach($options as $option){
            $lists_select[$option->id] = array();
            foreach($option->listvalues as $lst)
            {
                $lists_select[$option->id][$lst->key] = $lst->value;
            }

        }
        //echo '<pre/>';
        //print_r($lists_select);die;
        return View::make('algorithms.getedit')->with(
            array(
                'algorithm' => $algorithm,
                'algorithmtypes'=>$this->algorithmTypes,
                'optionTypes'=>$this->optionTypes,
                'lists_select'=>$lists_select
            )
        );
    }

    public function  postEdit($id = 0){

        $rules = array(
            'name'       => 'required',
            'type'       => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::route('algorithms.getedit', array('id'=>$id))
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {
            if(!$id)
                $algorithm = new Algorithm;
            else
                $algorithm = Algorithm::find($id);

            $algorithm->name = Input::get('name');
            $algorithm->description = Input::get('description');
            $algorithm->type = Input::get('type');
            $algorithm->save();
            // redirect
            //Session::flash('message', 'Алгоритм добавлен!');
            return Redirect::route('algorithms.getedit', array('id'=>$algorithm->id))
                ->withInput(Input::all());
        }
    }

    public  function  getDelete($id = 0){

        if($id){
            $algorithm = Algorithm::find($id);
            if($algorithm)
                $algorithm->delete();
        }

        return Redirect::route("algorithms.getindex");
    }

    public  function postBlockparam(){

        $option = new Algorithmoption();
        $option->name = 'Новый параметр';
        $option->symbol = 'Новый параметр';

        $algorithm_id = Input::get('algorithm_id');

        if(!$algorithm_id)
        {
            Response::json(array('error'=>'no algorithm id'));
        }

        $algorithm = Algorithm::find($algorithm_id);

        $algorithm->algorithmoptions()->save($option);

        return View::make('algorithms.partails.blockparam')
            ->with(
                array(
                    'block_id'=>''.$option->id,
                    'param_name' => 'Новый параметр',
                    'optionTypes' => $this->optionTypes,
                    'option'=>$option,
                    'lists_select'=>array($option->id => array())
                )
            );
    }

    public  function postBlockparamlist(){
        $list_id = time().'_l';
        $param_id = Input::get('param_id');
        $option = Algorithmoption::find($param_id);

        $list = new Listvalue();

        $list->value = 'value';
        $list->key = 'key';

        if($option)
            $option->listvalues()->save($list);

        return View::make('algorithms.partails.blockparamlist')
            ->with(
                array(
                    'list'=>$list
                )
            );
    }
    public  function postBlockparamdelete(){

        $param_id = Input::get('param_id');
        if(!$param_id){
            return Response::json(array('status'=>'0','error'=>1, 'error_text'=>'No ID'));
        }

        $options = Algorithmoption::find($param_id);

        if($options)
            $options->delete();

        return Response::json(array('status'=>'1','error'=>0, 'error_text'=>''));
    }

    public  function postBlockparamlistdelete(){

        $list_id= Input::get('list_id');

        if(!$list_id)
            return Response::json(array('status'=>'0','error'=>1, 'error_text'=>'No ID'));

        $list = Listvalue::find($list_id);
        if($list)
            $list->delete();

        return Response::json(array('status'=>'1','error'=>0, 'error_text'=>''));
    }

    public function postSaveParams(){

        $data = Input::get('data');

        $inputs = array();
        $list = array();
        if($data)
            foreach($data as $key => $value)
            {

                if($value)
                {
                    foreach($value as $param_name => $param_value)
                    {
                        $buf_arr = explode('_', $param_name);

                        if( in_array($buf_arr[0],array('key','value'))){
                            $list[$buf_arr[1]][$buf_arr[0]] = $param_value;
                        }else{
                            $inputs[$buf_arr[1]][$buf_arr[0]] = $param_value;
                        }

                    }
                }

            }

        print_r($inputs);
        print_r($list);

        if($inputs){
            foreach($inputs as $key=>$value){
                $algorithm_option = Algorithmoption::find($key);
                if($algorithm_option){
                    $algorithm_option->name = $value['name'];
                    $algorithm_option->symbol = $value['symbol'];
                    $algorithm_option->type = $value['type'];
                    $algorithm_option->min = $value['min'];
                    $algorithm_option->max = $value['max'];
                    $algorithm_option->default_double = $value['default0'];
                    $algorithm_option->default_list = $value['default2'];
                    $algorithm_option->default_string = $value['default3'];
                    $algorithm_option->default_bool = $value['default1'];
                    $algorithm_option->save();
                }
            }
        }

        if($list){
            foreach($list as $key=>$value){
                $list_val = Listvalue::find($key);
                if($list_val){
                    $list_val->key = $value['key'];
                    $list_val->value = $value['value'];
                    $list_val->save();
                }
            }
        }


        return Response::json(array('status'=>'1','error'=>0, 'error_text'=>''));
    }
}