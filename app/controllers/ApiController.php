<?php

class ApiController extends BaseController{

	public function get_project($id = 0){
        $response = new stdClass();
        $response->error = 0;
        $response->description = "id is empty";

        if (!$id) {
            return Response::json($response);
        }

        $project = Project::find($id);



        $algorithms = array();

        $items = AlgorithmProject::where("project_id","=",$id)->orderBy('sort')->get();
        $selected_options = Selectedoption::where("project_id","=",$id)->get();
        $soption = array();
        if($selected_options){
            foreach($selected_options as $option){
                $soption[$option->algorithmproject_id][$option->algorithmoption_id] = $option;
            }
        }

        if($items){
            foreach($items as $item){
                $algorithm = array();


                $algorithm['id'] = $item->algorithm_id;
                $algorithm['type'] = $item->algorithm->type;
                $algorithm['name'] = $item->algorithm->name;
                $algorithm['description'] = $item->algorithm->description;

                $algorithmoptions = $item->algorithm->algorithmoptions;

                $params = array();
                foreach($algorithmoptions as $option){


                        switch ($option->type) {
                            case 0:
                                $params[$option->symbol] = !empty($soption[$item->id][$option->id]) ? $soption[$item->id][$option->id]->double_value : '';
                                break;
                            case 1:
                                $params[$option->symbol] = !empty($soption[$item->id][$option->id]) ? $soption[$item->id][$option->id]->bool_value : '';
                                break;
                            case 2:
                                $params[$option->symbol] = !empty($soption[$item->id][$option->id]) ? $soption[$item->id][$option->id]->list_value : '';
                                break;
                            case 3:
                                $params[$option->symbol] = !empty($soption[$item->id][$option->id]) ? $soption[$item->id][$option->id]->string_value : '';
                                break;
                        }

                }

                $algorithm['params'] = $params;
                $algorithms[] = $algorithm;
            }
        }


        $object = array();

        $object['id'] = $project->id;
        $object['state'] = $project->state;
        $object['name'] = $project->name;
        $object['user_id'] = $project->user_id;
        $object['cycles'] = $project->cycles;
        $object['functionType'] = $project->functionType;
        $object['projecttype_id'] = $project->projecttype_id;
        $object['total_rows_tra'] = $project->total_rows_tra;
        $object['slognost'] = $project->slognost ? json_decode($project->slognost) : '';
        $object['selected_slognost'] = $project->selected_slognost ? json_decode($project->selected_slognost) : '';
        $object['description'] = $project->description;
        $object['table_tst'] = URL::asset('tables/'.$project->table_tst);
        $object['table_tra'] = URL::asset('tables/'.$project->table_tra);
        $object['ufs_file'] = $project->ufsfiles->toArray();
        $object['algorithms'] = $algorithms;

        return Response::json($object);

	}

    public function update_project() {

        //print_r(Request::getMethod());
        //print_r(Input::all());
        $resp = new stdClass();

        $data = Input::get('data');
        if (empty($data)) {
            $resp->error = "no data";
            $resp->status = 0;
            return Response::json($resp);
        }

        $obj = json_decode($data);

        if (empty($obj->action))
        {
            $resp->error = "no action";
            $resp->status = 0;
            return Response::json($resp);
        }

        switch ($obj->action) {

            case 'setstatus':
                if (!isset($obj->projectstatus))
                {
                    $resp->error = "no status";
                    $resp->status = 0;
                    return Response::json($resp);
                }
                if (empty($obj->projectid))
                {
                    $resp->error = "no project id";
                    $resp->status = 0;
                    return Response::json($resp);
                }

                $project = Project::find($obj->projectid);
                $project->state = $obj->projectstatus;

                if ($project->save()) {
                    $resp->error = 0;
                    $resp->status = 1;
                    return Response::json($resp);
                } else {
                    $resp->error = "save project status error. project id = ". $obj->projectid;
                    $resp->status = 0;
                    return Response::json($resp);
                }
                break;
            case 'getprojectbystatus':
                $resp = new stdClass();
                if (!isset($obj->projectstatus))
                {
                    $resp->error = "no status";
                    $resp->status = 0;
                    return Response::json($resp);
                }

                $projects = Project:: where('state','=', $obj->projectstatus)->get();

                $objects = array();
                foreach($projects as $project){
                    $object = array();

                    $object['id'] = $project->id;
                    $object['state'] = $project->state;
                    $object['name'] = $project->name;
                    $object['user_id'] = $project->user_id;
                    $object['cycles'] = $project->cycles;
                    $object['functionType'] = $project->functionType;
                    $object['projecttype_id'] = $project->projecttype_id;
                    $object['total_rows_tra'] = $project->total_rows_tra;
                    $object['slognost'] = $project->slognost ? json_decode($project->slognost) : '';
                    $object['selected_slognost'] = $project->selected_slognost ? json_decode($project->selected_slognost) : '';
                    $object['description'] = $project->description;
                    $object['table_tst'] = URL::asset('tables/'.$project->table_tst);
                    $object['table_tra'] = URL::asset('tables/'.$project->table_tra);
                    $object['ufs_file'] = $project->ufsfiles->toArray();
                    $objects[] = array('Project'=>$object);
                }
                return Response::json($objects);
                break;
        }
        $resp->error = "empty error";
        $resp->status = 0;
        return Response::json($resp);
    }

    public function upload_ufs($id = NULL) {


        $resp = new stdClass();
        $id = intval($id);
        if (!$id){
            $resp->error = "no project id";
            $resp->status = 0;
            return Response::json($resp);
        }

        $destinationPath = public_path() . $this->upload_dir . $id;
        $res = true;
        if(!File::exists($destinationPath))
            File::makeDirectory($destinationPath, 0777, true, true);



        if(!$res){
            $resp->error = "directory creation error";
            $resp->status = 0;
            return Response::json($resp);
        }

        if (!Input::hasFile('file')){
            $resp->error = "no file";
            $resp->status = 0;
            return Response::json($resp);
        }


        if(Input::hasFile('file')){
            $file            = Input::file('file');
            $filename        = time().'-'.$file->getClientOriginalName();
            $uploadSuccess   = $file->move($destinationPath, $filename);
            if($uploadSuccess){
                $project = Project::find($id);
                if($project->projecttype_id!=4){
                $xml = new DOMDocument();
                $xml->load($destinationPath.'/'.$filename);

                $nodes  = $xml->getElementsByTagName('Estimate');
                $tra_tochnost = doubleval($nodes->item(0)->getAttribute('Value'));
                $tst_tochnost = doubleval($nodes->item(1)->getAttribute('Value'));

                $buf =  explode(".", $filename);
                $buf = explode("_",$buf[0]);
                $buf = explode(",", $buf[3]);
                $pr = 1;
                $s = 0;
                foreach($buf as $b){
                    $pr*=(intval($b));
                    $s+=(intval($b));
                }

                $sl = $s+$pr;
                }else{
                    $tra_tochnost =-1;
                    $tst_tochnost=-1;
                    $sl = -1;
                }
                $ufs_file = new Ufsfile();
                $ufs_file->tra_tochnost = $tra_tochnost;
                $ufs_file->tst_tochnost = $tst_tochnost;
                $ufs_file->tra_slognost = $sl;
                $ufs_file->tst_slognost = $sl;
                $ufs_file->file = $filename;

                if($project){

                    $project->ufsfiles()->save($ufs_file);
                }

                $resp->error = "";
                $resp->status = 0;
                return Response::json($resp);
            }else{
                $resp->error = "upload failed";
                $resp->status = 1;
                return Response::json($resp);
            }
        }
    }
}