<ul class="nav navbar-top-links navbar-right">
    <!-- /.dropdown -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            {{$user->last_name.' '.$user->first_name}} <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="fa fa-user fa-fw"></i> Профиль</a>
            </li>
            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Настройки</a>
            </li>
            <li class="divider"></li>
            <li><a href="{{URL::route('users.logout')}}"><i class="fa fa-sign-out fa-fw"></i> Выйти</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->