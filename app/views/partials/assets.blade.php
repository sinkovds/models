<!-- Core CSS - Include with every page -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Page-Level Plugin CSS - Blank -->

<!-- SB Admin CSS - Include with every page -->
<link href="{{ URL::asset('assets/css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/styles.css') }}" rel="stylesheet">




<!-- Core Scripts - Include with every page -->
<script src="{{ URL::asset('assets/js/jquery-1.10.2.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.cookie.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

<!-- Page-Level Plugin Scripts - Blank -->


<!-- Page-Level Plugin Scripts - Flot -->
<!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
<script src="{{ URL::asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>


<!-- SB Admin Scripts - Include with every page -->
<script src="{{ URL::asset('assets/js/sb-admin.js') }}"></script>

<!-- Page-Level Demo Scripts - Blank - Use for reference -->

<script src="{{ URL::asset('assets/js/main.js') }}"></script>
