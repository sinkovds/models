<div class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ URL::route('projects.getindex') }}"><i class="fa fa-tasks fa-fw"></i> Проекты</a>
            </li>
            <li>
                <a href="{{ URL::route('algorithms.getindex') }}"><i class="fa fa-cogs fa-fw"></i> Алгоритмы</a>
            </li>
        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->