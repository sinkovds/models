@extends('layouts.default');

@section('main')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            {{{ isset($algorithm->id) ? 'Редактирование' : 'Добавление' }}} алгоритма</h1>


            <div class="col-lg-12">
                <div class="panel panel-default">
                    {{ Form::model($algorithm, array('route' => array('algorithms.postedit', $algorithm->id))) }}
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab_description">Описание алгоритма</a></li>
                                @if(!empty($algorithm->id))
                                    <li class=""><a data-toggle="tab" href="#tab_params">Параметры алгоритма</a></li>
                                @endif
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="tab_description" class="tab-pane fade active in">
                                    {{ HTML::ul($errors->all()) }}

                                    <div class="form-group">
                                        {{ Form::label('name', 'Название')}}
                                        {{ Form::text('name',$algorithm->name, array('class'=>'form-control'))}}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('description', 'Описание')}}
                                        {{ Form::textarea('description',$algorithm->description, array('class'=>'form-control'))}}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('type', 'Тип алгоритма')}}
                                        {{ Form::select('type', $algorithmtypes, $algorithm->type, array('class'=>'form-control'))}}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::submit('Сохранить',array('class'=>'btn btn-default'))}}
                                    </div>
                                </div>
                                @if(!empty($algorithm->id))
                                    <div id="tab_params" class="tab-pane fade">
                                        <div class="row">
                                            <div id="param_add_panel">
                                                <a algorithm_id="{{$algorithm->id}}" href={{URL::route('algorithms.blockparam')}} id="add_param_button" class="btn btn-default" type="button">Добавить параметр</a>
                                                <a id="save_param_button" href={{URL::route('algorithms.saveparam')}} class="btn btn-default" type="button">Сохранить</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <!-- .panel-heading -->
                                                    <div class="panel-body">
                                                        <div class="panel-group" id="accordion">
                                                        @foreach ($algorithm->algorithmoptions as $option)
                                                            {{
                                                                View::make('algorithms.partails.blockparam')
                                                                        ->with(
                                                                            array(
                                                                                'block_id'=>''.$option->id,
                                                                                'param_name' => $option->name,
                                                                                'optionTypes' => $optionTypes,
                                                                                'option'=>$option,
                                                                                'lists_select'=>$lists_select
                                                                            )
                                                                        );
                                                            }}
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- .panel-body -->
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                            <!-- /.col-lg-12 -->
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    {{ Form::close() }}
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
    </div>
@stop