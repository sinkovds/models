@extends('layouts.default');

@section('main')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Алгоритмы</h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Алгоритмы генерации правил <a href="{{ URL::route('algorithms.getedit') }}" class="btn btn-default btn-circle add_alg_bnt" type="button"><i class="fa fa-plus"></i></a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Операции</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($algorithms[0]))
                                @foreach($algorithms[0] as $algorithm)
                                    <tr>
                                        <td>{{$algorithm->id}}</td>
                                        <td>{{$algorithm->name}}</td>
                                        <td>
                                            <a href="{{ URL::route('algorithms.getedit', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ URL::route('algorithms.getdelete', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>

            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

    </div>

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Алгоритмы оптимизации <a href="{{ URL::route('algorithms.getedit') }}"  class="btn btn-default btn-circle add_alg_bnt" type="button"><i class="fa fa-plus"></i></a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Операции</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($algorithms[1]))
                                @foreach($algorithms[1] as $algorithm)
                                    <tr>
                                        <td>{{$algorithm->id}}</td>
                                        <td>{{$algorithm->name}}</td>
                                        <td>
                                            <a href="{{ URL::route('algorithms.getedit', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ URL::route('algorithms.getdelete', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Алгоритмы оптимизации классификации <a href="{{ URL::route('algorithms.getedit') }}"  class="btn btn-default btn-circle add_alg_bnt" type="button"><i class="fa fa-plus"></i></a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Операции</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($algorithms[2]))
                                @foreach($algorithms[2] as $algorithm)
                                    <tr>
                                        <td>{{$algorithm->id}}</td>
                                        <td>{{$algorithm->name}}</td>
                                        <td>
                                            <a href="{{ URL::route('algorithms.getedit', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ URL::route('algorithms.getdelete', array('id' => $algorithm->id)) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

</div>
<!-- /.row -->
@stop