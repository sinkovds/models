<div class="keys_block" lid="{{$list->id}}">
    <div class="form-group">
        {{ Form::label('key_'.$list->id, 'Ключ')}}
        {{ Form::Text('key_'.$list->id, $list->key, array('class'=>'form-control _key'))}}
    </div>
    <div class="form-group">
        {{ Form::label('value_', 'Значение')}}
        {{ Form::Text('value_'.$list->id, $list->value, array('class'=>'form-control _value'))}}
    </div>
    <div class="form-group">
        {{ Form::Button('Удалить', array('href'=>URL::route('algorithms.blockparamlistdelete'),'list_id'=>$list->id,'class'=>'btn btn-default del_list_val'))}}
    </div>
</div>