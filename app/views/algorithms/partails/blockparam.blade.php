<div class="panel panel-default item_prm" param_id="{{$block_id}}">
    <div class="panel-heading">
        <div class="caption_param">
            <h4 class="panel-title">
                <a id="caption_{{$block_id}}" data-toggle="collapse" data-parent="#accordion" href="#tb_{{$block_id}}">{{$param_name}}</a>
                <a id="delete_{{$block_id}}" href="{{URL::route('algorithms.blockparamdelete')}}"  class="btn btn-default btn-circle del_param_bnt" type="button"><i class="fa fa-times"></i></a>
            </h4>
        </div>
    </div>
    <div pid="{{$block_id}}" id="tb_{{$block_id}}" class="params_block panel-collapse collapse">
        <div class="panel-body">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('name_'.$block_id, 'Название')}}
                    {{ Form::text('name_'.$block_id, $option->name, array('param_id'=>$block_id,'param'=>'name','id'=>'name_'.$block_id ,'class'=>'prm_name form-control'))}}
                </div>
                <div class="form-group">
                    {{ Form::label('symbol_'.$block_id, 'Обозначение')}}
                    {{ Form::text('symbol_'.$block_id, $option->symbol, array('param'=>'symbol','id'=>'symbol_'.$block_id ,'class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    {{ Form::label('type_'.$block_id, 'Тип параметра')}}
                    {{ Form::select('type_'.$block_id, $optionTypes, $option->type, array('class'=>'type_param form-control'))}}
                </div>
            </div>
            <div id="param_type_0_{{$block_id}}" class="tiles_{{$block_id}} col-lg-6">
                <div class="form-group">
                    {{ Form::label('min_'.$block_id, 'Минимальное значение')}}
                    {{ Form::text('min_'.$block_id, $option->min, array('param'=>'min','id'=>'min_'.$block_id ,'class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    {{ Form::label('max_'.$block_id, 'Максимальное значение')}}
                    {{ Form::text('max_'.$block_id, $option->max, array('param'=>'max','id'=>'max_'.$block_id ,'class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    {{ Form::label('default0_'.$block_id, 'Значение по умолчанию')}}
                    {{ Form::text('default0_'.$block_id, $option->default_double, array('param'=>'default_0','id'=>'default0_'.$block_id ,'class'=>'form-control'))}}
                </div>
            </div>

            <div id="param_type_1_{{$block_id}}" class="tiles_{{$block_id}} col-lg-6">
                <div class="form-group">
                    {{ Form::label('default1_'.$block_id, 'Значение по умолчанию')}}
                    {{ Form::checkbox('default1_'.$block_id, $option->default_bool, array('param'=>'default_1','id'=>'default1_'.$block_id ,'class'=>'form-control'))}}
                </div>
            </div>
            <div id="param_type_3_{{$block_id}}" class="tiles_{{$block_id}} col-lg-6">
                <div class="form-group">
                    {{ Form::label('default3_'.$block_id, 'Значение по умолчанию')}}
                    {{ Form::text('default3_'.$block_id, $option->default_string, array('param'=>'default_3','id'=>'default3_'.$block_id ,'class'=>'form-control'))}}
                </div>
            </div>
            <div id="param_type_2_{{$block_id}}" class="tiles_{{$block_id}} col-lg-6">
                <div id="list_values_{{$block_id}}">
                    @foreach ($option->listvalues as $list)
                        {{
                            View::make('algorithms.partails.blockparamlist')
                                        ->with(
                                            array(
                                                'list'=>$list
                                            )
                                        );
                        }}
                    @endforeach
                </div>
                <div class="control_list">
                    <a param_id="{{$block_id}}" href="{{URL::route('algorithms.blockparamlist')}}" class="add_list btn btn-default btn-circle" type="button"><i class="fa fa-plus"></i></a>
                </div>
                <div class="form-group">
                    {{ Form::label('default2_'.$block_id, 'Значение по умолчанию')}}
                    {{ Form::select('default2_'.$block_id, $lists_select[$option->id], $option->default_list, array('param'=>'default_2','id'=>'default2_'.$block_id ,'class'=>'form-control'))}}
                </div>
            </div>

        </div>
    </div>
</div>