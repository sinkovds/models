@extends('layouts.login')

@section('main')
	<div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Регистрация</h3>
                </div>
                <div class="panel-body">
                    <form action="{{URL::route('users.register')}}" method="POST">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input name="email" type="email" autofocus class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input name="password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Повторите пароль</label>
                            <input name="dpassword" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Фамилия</label>
                            <input name="last_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Имя</label>
                            <input name="first_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <input name="submit" value="Зарегистрироваться" type="submit" class="btn btn-lg btn-success btn-block">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop