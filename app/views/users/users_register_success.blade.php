@extends('layouts.login')

@section('main')
	<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Регистрация прошла успешно</h3>
            <p>Перейдите к странице <a href="{{URL::route('users.login')}}" >входа</a> в систему</p>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@stop