@extends('layouts.login')

@section('main')
<div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Введите логин и пароль</h3>
                    </div>
                    <div class="panel-body">

                            <fieldset>
                                <form action="{{ URL::route('users.login')}}" method="POST" role="form">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                    <div style="display:none;" class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="Remember Me">Не выходить из системы
                                        </label>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" value="Войти" href="" class="btn btn-lg btn-success btn-block"/>
                                </form>
                                <a href="{{ URL::route('users.register')}}" class="btn btn-block btn-outline btn-link" >Регистрация</a>
                            </fieldset>

                    </div>
                </div>
            </div>
        </div>
@stop