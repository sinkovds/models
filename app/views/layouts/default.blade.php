<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Система нечеткого моделирования</title>

    @include('partials.assets')

</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        @include('partials.navbarheader')
        @include('partials.toplinks')
        @include('partials.leftmenu')
    </nav>
    <div id="page-wrapper">
        @yield('main')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
</body>

</html>
