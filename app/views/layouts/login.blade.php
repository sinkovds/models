<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Система нечеткого моделирования</title>
    @include('partials.assets')
    <style  type="text/css">
        body {
            background: url({{ URL::asset('assets/img/bg2.jpg')}}) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>
<body>

    <div class="container">
        @yield('main')
    </div>
</body>

</html>
