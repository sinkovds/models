@if($project->id)
    <div id="tab_table" class="tab-pane fade">
        <div class="proj_div">
            <form  enctype="multipart/form-data" method="POST" action="{{URL::route('projects.loadtables',array('id'=>$project->id))}}">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Обучающая таблица наблюдений
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                @if($project->table_tra)
                                    <div class="form-group">
                                        <p class="form-control-static">
                                            <a target="_blank" href="{{ URL::asset('tables/'.$project->table_tra) }}">{{$project->table_tra}}</a>
                                            <button href="{{URL::route('projects.deletetable',array('id'=>$project->id, 'type'=>'tra'))}}" class="btn pr_btn btn-default btn-circle" type="button"><i class="fa fa-times"></i></button>
                                        </p>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Выбирете файл (KELL формат)</label>
                                    <input type="file" name="tra_data" id="tra_data">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-default" name="submit" value = "Загрузить">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Тестовая таблица наблюдений
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @if($project->table_tst)
                                <div class="form-group">
                                    <p class="form-control-static">
                                        <a target="_blank"  href="{{ URL::asset('tables/'.$project->table_tst) }}">{{$project->table_tst}}</a>
                                        <button href="{{URL::route('projects.deletetable',array('id'=>$project->id, 'type'=>'tst'))}}" class="btn pr_btn btn-default btn-circle" type="button"><i class="fa fa-times"></i></button>
                                    </p>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Выбирете файл (KELL формат)</label>
                                <input type="file" name="tst_data" id="tst_data">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-default" name="submit" value = "Загрузить">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
        @if($project->table_tra && $project->table_tst)
            <div id="tab_algorithms" class="tab-pane fade">
                <div class="proj_div">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Сложность системы
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <form method="POST" action="{{URL::route('projects.slognost',array('id'=>$project->id))}}">
                                    <div class="form-group">
                                        @foreach($slognost as $key=>$value)
                                            @foreach($value as $val)
                                            <label class="checkbox-inline">
                                                <input name='slognost[]' {{ !empty($selected_slognost) && in_array($val, $selected_slognost) ? 'checked="checked"' : '' }} value="{{$val}}" type="checkbox">{{$key}}({{$val}})
                                            </label>
                                            @endforeach
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" name="submit" value = "Сохранить">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Алгоритмы оптимизации
                            </div>
                            <!-- /.panel-heading -->

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel-body">

                                            @if(count($alg_project))
                                            <div id="accordion" class="panel-group">
                                                @foreach($alg_project as $item)
                                                    <form method="POST" action="{{URL::route('projects.savealgparam',array('id'=>$project->id,'itemid'=>$item->id))}}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading salg">
                                                            <h4 class="panel-title">
                                                                <a href="#alg_{{$item->id}}" data-parent="#accordion" data-toggle="collapse" class="collapsed">{{$item->algorithm->name}}</a>
                                                            </h4>
                                                            <a type="button" class="btn btn-default btn-circle options_btn" href="{{URL::route('projects.delalg',array('id'=>$project->id, 'itemid'=>$item->id))}}"><i class="fa fa-times"></i></a>
                                                            <a type="button" class="btn btn-default btn-circle options_btn" href="{{URL::route('projects.resort',array('id'=>$project->id, 'itemid'=>$item->id,'type'=>'down'))}}"><i class="fa fa-chevron-down"></i></a>
                                                            <a type="button" class="btn btn-default btn-circle options_btn" href="{{URL::route('projects.resort',array('id'=>$project->id, 'itemid'=>$item->id,'type'=>'up'))}}"><i class="fa fa-chevron-up"></i></a>
                                                        </div>
                                                        <div class="panel-collapse collapse" id="alg_{{$item->id}}">
                                                            <div class="panel-body">
                                                                @foreach($item->algorithm->algorithmoptions as $option)
                                                                    @if($option->type==0)
                                                                        <div class="form-group">
                                                                            <label>{{$option->symbol}}</label>
                                                                            <input type="text" name="{{'opt_'.$option->id}}" id="{{'opt_'.$option->id}}" value="{{ isset($sel_vl[$project->id][$item->id][$option->id]) ? $sel_vl[$project->id][$item->id][$option->id]->double_value :  $option->default_double}}" class="form-control"/>
                                                                            <p class="help-block">{{$option->name}}</p>
                                                                        </div>
                                                                    @endif

                                                                    @if($option->type==1)
                                                                        <div class="form-group">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" name="{{'opt_'.$option->id}}" id="{{'opt_'.$option->id}}" {{ ( isset($sel_vl[$project->id][$item->id][$option->id]) ? $sel_vl[$project->id][$item->id][$option->id]->bool_value :  $option->default_bool) ? 'checked="checked"' : ''}} />
                                                                                    {{$option->symbol}}
                                                                                </label>
                                                                            </div>
                                                                            <p class="help-block">{{$option->name}}</p>
                                                                        </div>
                                                                    @endif

                                                                    @if($option->type==2)
                                                                        <div class="form-group">
                                                                            <label>{{$option->symbol}}</label>
                                                                            <select name="{{'opt_'.$option->id}}" class="form-control">
                                                                                @foreach($option->listvalues as $list)
                                                                                   <option {{ $list->key == (isset($sel_vl[$project->id][$item->id][$option->id]) ? $sel_vl[$project->id][$item->id][$option->id]->list_value : $option->default_list) ? 'selected' : '' }} value="{{$list->key}}">{{$list->value}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    @endif

                                                                    @if($option->type==3)
                                                                        <div class="form-group">
                                                                            <label>{{$option->symbol}}</label>
                                                                            <input type="text" name="{{'opt_'.$option->id}}" id="{{'opt_'.$option->id}}" value="{{isset($sel_vl[$project->id][$item->id][$option->id]) ? $sel_vl[$project->id][$item->id][$option->id]->string_value : $option->default_string}}" class="form-control"/>
                                                                            <p class="help-block">{{$option->name}}</p>
                                                                        </div>
                                                                    @endif

                                                                @endforeach
                                                                <div class="form-group">
                                                                    <input type="submit" class="btn btn-default" name="submit" value = "Сохранить">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                @endforeach
                                            </div>

                                            @else
                                                <p>Алгоритмы еще не выбраны</p>
                                            @endif
                                        </div>

                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>

                                    <form action="{{URL::route('projects.addalg',array('id'=>$project->id))}}" method="POST">
                                        <div class="form-group">
                                            {{ Form::label('algorithm', 'Алгоритм оптимизации')}}
                                            {{ Form::select('algorithm_id', $algorithms, 0, array('class'=>'form-control'))}}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::submit('Добавить',array('class'=>'btn btn-default'))}}
                                        </div>
                                    </form>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Параметры оптимизации
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <form method="POST" action="{{URL::route('projects.optimizationparams',array('id'=>$project->id))}}">
                                    <div class="form-group">
                                        <label>Циклов оптимизации</label>
                                        <input type="text" name="cycles" id="cycles" value="{{$project->cycles}}" class="form-control"/>
                                        <p class="help-block">Сколько раз будет повторяться заданная цепочка алгоритмов на каждой сложности</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" name="submit" value = "Сохранить">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Тип функций принадлежности
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <form method="POST" action="{{URL::route('projects.functiontype',array('id'=>$project->id))}}">
                                    <div class="form-group">
                                        {{ Form::label('functionType', 'Тип функции принадлежности')}}
                                        {{ Form::select('functionType', $functionTypes, $project->functionType, array('class'=>'form-control'))}}
                                        <p class="help-block">Тип функций принадлежности в правилах</p>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::submit('Сохранить',array('class'=>'btn btn-default'))}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab_use" class="tab-pane fade">
            <script>
                var plot_point = {{$plot_point}};

                $(document).ready(function() {
                    var offset = 0;
                    plot();

                    function plot() {
                        var tra = plot_point?plot_point[1]:[],
                            tra_pareto = plot_point?plot_point[3]:[],
                            tst = plot_point?plot_point[2]:[],
                            tst_pareto = plot_point?plot_point[4]:[];

                        var options = {
                            series: {
                                lines: {
                                    show: false
                                },
                                points: {
                                    show: true
                                }
                            },
                            grid: {
                                hoverable: true //IMPORTANT! this is needed for tooltip to work
                            },
                            yaxis: {
                                //min: -1.2,
                                //max: 1.2
                            },
                            tooltip: true,
                            tooltipOpts: {
                                content: "'%s': сложность %x, точность %y",
                                shifts: {
                                    x: -60,
                                    y: 25
                                }
                            }
                        };

                        var plotObj = $.plot($("#flot-line-chart"), [{
                                data: tra,
                                label: "Обучающая выборка"
                            }, {
                                data: tst,
                                label: "Тестовая выборка"
                            }],
                            options);


                        var options_pareto = {
                                        series: {
                                            lines: {
                                                show: true
                                            },
                                            points: {
                                                show: true
                                            }
                                        },
                                        grid: {
                                            hoverable: true //IMPORTANT! this is needed for tooltip to work
                                        },
                                        yaxis: {
                                            //min: -1.2,
                                            //max: 1.2
                                        },
                                        tooltip: true,
                                        tooltipOpts: {
                                            content: "'%s': сложность %x, точность %y",
                                            shifts: {
                                                x: -60,
                                                y: 25
                                            }
                                        }
                                    };
                        var plotObj_pareto = $.plot($("#flot-line-chart_pareto"), [{
                                                        data: tra_pareto,
                                                        label: "Обучающая выборка"
                                                    }, {
                                                        data: tst_pareto,
                                                        label: "Тестовая выборка"
                                                    }],
                                                    options_pareto);
                    }
                });
            </script>
                <div class="proj_div">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                   Все системы
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-line-chart"></div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                   Парето фронт
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-line-chart_pareto"></div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Оптимизированные системы
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Сложность</th>
                                                <th>Точность (tra)</th>
                                                <th>Точность (tst)</th>
                                                <th class="option_td">Опции</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($project->ufsfiles)
                                                @foreach($project->ufsfiles as $file)
                                                    <tr>
                                                        <td>{{$file->tra_slognost}}</td>
                                                        <td>{{$file->tra_tochnost}}</td>
                                                        <td>{{$file->tst_tochnost}}</td>
                                                        <td>
                                                            <a  target="_blank"  href="{{ URL::asset('systems_ufs/'.$project->id.'/'.$file->file) }}" class="btn btn-default btn-circle" type="button"><i class="fa fa-cloud-download"></i></a>
                                                            <button href="{{URL::route('projects.use_system',array('id'=>$file->id))}}" class="btn btn-default btn-circle pr_btn" type="button"><i class="fa fa-play"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
            </div>
        @endif
    @endif