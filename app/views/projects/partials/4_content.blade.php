@if($project->id)
    <div id="tab_table" class="tab-pane fade">
        <div class="proj_div">
            <form  enctype="multipart/form-data" method="POST" action="{{URL::route('projects.loadtables',array('id'=>$project->id))}}">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Данные истории продаж
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                @if($project->table_tra)
                                    <div class="form-group">
                                        <p class="form-control-static">
                                            <a target="_blank" href="{{ URL::asset('tables/'.$project->table_tra) }}">{{$project->table_tra}}</a>
                                            <button href="{{URL::route('projects.deletetable',array('id'=>$project->id, 'type'=>'tra'))}}" class="btn pr_btn btn-default btn-circle" type="button"><i class="fa fa-times"></i></button>
                                        </p>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Выбирете файл</label>
                                    <input type="file" name="tra_data" id="tra_data">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-default" name="submit" value = "Загрузить">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Загрузка продукции
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @if($project->table_tst)
                                <div class="form-group">
                                    <p class="form-control-static">
                                        <a target="_blank" href="{{ URL::asset('tables/'.$project->table_tst) }}">{{$project->table_tst}}</a>
                                        <button href="{{URL::route('projects.deletetable',array('id'=>$project->id, 'type'=>'tst'))}}" class="btn pr_btn btn-default btn-circle" type="button"><i class="fa fa-times"></i></button>
                                    </p>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Выбирете файл</label>
                                <input type="file" name="tst_data" id="tst_data">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-default" name="submit" value = "Загрузить">
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
        @if($project->table_tra)
            <div id="tab_algorithms" class="tab-pane fade">
                <div class="proj_div">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Ассоциативные правила
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif