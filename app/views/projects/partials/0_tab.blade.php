@if($project->id)
    <li><a data-toggle="tab" href="#tab_table">Таблица наблюдений</a></li>
    @if($project->table_tra && $project->table_tst)
        <li><a data-toggle="tab" href="#tab_algorithms">Построение модели</a></li>
        <li><a data-toggle="tab" href="#tab_use">Использование</a></li>
    @endif
@endif