@extends('layouts.default');

@section('main')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p>Оптимизированная система проекта: <a href="{{URL::route('projects.getedit',array('id'=>$ufsfile->project->id))}}">{{$ufsfile->project->name}}</a></p>
                <p>Файл системы: <a  target="_blank"  href="{{ URL::asset('systems_ufs/'.$ufsfile->project->id.'/'.$ufsfile->file) }}" type="button">{{$ufsfile->file}}</a></p>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @if(isset($fuzzy_systems->variables))
                    <form name="fuzzyvars" id="fuzzyvars">
                        @foreach($fuzzy_systems->variables as $variable)
                        <div class="form-group">
                            {{ Form::label($variable->name, $variable->name)}}
                            {{ Form::text($variable->name, ($variable->min+$variable->max)/2, array('class'=>'form-control'))}}
                            <p class="help-block">Область определения: [{{$variable->min}};{{$variable->max}}]</p>
                        </div>
                        @endforeach
                    </form>
                    <div class="form-group">
                        {{ Form::button('Рассчитать',array('class'=>'btn btn-default','href'=>URL::route('projects.use_system',array('id'=>$ufsfile->id)), 'id'=>'usebtn'))}}
                    </div>
                @endif
            </div>
            <!-- /.panel-body -->
            <div id="result_content" class="panel-footer">

            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
@stop