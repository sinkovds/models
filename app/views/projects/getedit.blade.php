@extends('layouts.default')

@section('main')
<div class="row">
    <div class="col-lg-12">
            <h1 class="page-header">
                {{{ isset($project->id) ? 'Редактирование' : 'Добавление' }}} проекта</h1>


                <div class="col-lg-12">
                    <div class="panel panel-default">

                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab_description">Описание проекта</a></li>
                                    @include('projects.partials.'.$template_id.'_tab')
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="tab_description" class="tab-pane fade active in">
                                        {{ Form::model($project, array('route' => array('projects.postedit', $project->id))) }}
                                        {{ HTML::ul($errors->all()) }}

                                        <div class="form-group">
                                            {{ Form::label('name', 'Название')}}
                                            {{ Form::text('name',null, array('class'=>'form-control'))}}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('description', 'Описание')}}
                                            {{ Form::textarea('description',null, array('class'=>'form-control'))}}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::submit('Сохранить',array('class'=>'btn btn-default'))}}
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    @include('projects.partials.'.$template_id.'_content')
                                </div>
                            </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
        </div>
</div>
<!-- /.row -->
@stop