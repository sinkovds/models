@extends('layouts.default');

@section('main')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Проекты
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-pills">
                @foreach ($pojectStatuses as $key=>$value)
                    <li  {{ $key==0 ? 'class = "active"' : ''}}><a data-toggle="tab" href="#status_{{$key}}">{{$value}} ({{isset($projects[$key]) ? count($projects[$key]) : 0}})</a></li>
                @endforeach
                </ul>
                <!-- Tab panes -->
                <div class="tab-content proj_div">
                    @foreach ($pojectStatuses as $key=>$value)
                        <div id="status_{{$key}}" class="tab-pane fade {{ $key==0 ? 'in active' : ''}}">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="number_td">№</th>
                                                    <th>Название</th>
                                                    <th>Тип</th>
                                                    <th  class="option_td">Опции</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($projects[$key]))
                                                @foreach ($projects[$key] as $project)
                                                <tr>
                                                    <td>{{$project->id}}</td>
                                                    <td>{{$project->name}}</td>
                                                    <td>{{$projectTypes[$project->projecttype_id]}}</td>
                                                    <td>
                                                        @if(!in_array($project->state, array(-1,1)))
                                                            <button href="{{ URL::route('projects.getedit',array('id'=>$project->id))}}" class="btn btn-default btn-circle pr_btn" type="button"><i class="fa fa-pencil"></i></button>
                                                            <button href="{{ URL::route('projects.getdelete',array('id'=>$project->id))}}" class="btn btn-default btn-circle pr_btn" type="button"><i class="fa fa-trash-o"></i></button>
                                                            <button href="{{ URL::route('projects.getsetstate',array('id'=>$project->id,'state'=>'1'))}}" class="btn btn-default btn-circle pr_btn" type="button"><i class="fa fa-play"></i></button>
                                                        @else
                                                            <button  href="{{ URL::route('projects.getsetstate',array('id'=>$project->id,'state'=>'0'))}}"class="btn btn-default btn-circle pr_btn" type="button"><i class="fa fa-stop"></i></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td align="center" colspan="3">Проектов не найдено</td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <form action="{{URL::route('projects.postedit')}}" method="POST">
                    {{Form::select('projecttype_id',$projectTypes,0,array('class'=>'form-control select_add'))}}
                    <input class="btn btn-default" value="Новый проект" type="submit"/>
                </form>
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
@stop