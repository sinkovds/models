<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class Algorithmoption extends Eloquent {
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::deleted(function($option)
        {
            $ids = $option->listvalues()->lists('id');
            if($ids)
                Listvalue::destroy($ids);
        });

    }

    public function algorithm()
    {
        return $this->belongsTo('Algorithm');
    }

    public function listvalues()
    {
        return $this->hasMany('Listvalue');
    }
} 