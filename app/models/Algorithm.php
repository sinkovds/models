<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class Algorithm extends Eloquent {
    protected $guarded = array('id');
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::deleted(function($algorithm)
        {
            $ids = $algorithm->algorithmoptions()->lists('id');
            if($ids)
                Algorithmoption::destroy($ids);
        });

    }

    public function projects()
    {
        return $this->belongsToMany('Project');
    }

    public function algorithmoptions()
    {
        return $this->hasMany('Algorithmoption');
    }
} 