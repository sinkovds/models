<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class Project extends Eloquent {
    public function algorithms()
    {
        return $this->belongsToMany('Algorithm');
    }

    public function algorithmprojects()
    {
        return $this->hasMany('AlgorithmProject');
    }

    public function selectedoptions()
    {
        return $this->hasMany('Selectedoption');
    }


    public function ufsfiles()
    {
        return $this->hasMany('Ufsfile')->orderBy('tra_slognost');
    }
}