<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class Ufsfile extends Eloquent {
    protected $guarded = array('id');
    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('Project');
    }

} 