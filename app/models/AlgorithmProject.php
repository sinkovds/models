<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class AlgorithmProject extends Eloquent {
    protected $guarded = array('id');
    public $timestamps = false;
    protected $table = 'algorithm_project';

    public function algorithm()
    {
        return $this->belongsTo('Algorithm');
    }

    public function project()
    {
        return $this->belongsTo('Project');
    }

    public function selectedoptions()
    {
        return $this->hasMany('Selectedoption', 'algorithmproject_id');
    }
}