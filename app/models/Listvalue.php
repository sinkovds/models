<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 19:33
 */

class Listvalue extends Eloquent {
    public $timestamps = false;
    public function algorithmoption()
    {
        return $this->belongsTo('Algorithmoption');
    }
}