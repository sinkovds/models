<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('projects', function($table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name', 1024);
            $table->integer('state');
            $table->text('description');
            $table->string('table_tra', 1024);
            $table->string('table_tst', 1024);
            $table->string('ufs_file', 1024);
            $table->text('result');
            $table->text('slognost');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('projects');
	}

}
