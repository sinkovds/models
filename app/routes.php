<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', array('as' => 'users.login', 'uses' => 'UsersController@getLogin'));
Route::post('login', array('as' => 'users.login', 'uses' => 'UsersController@postLogin'));
Route::get('register', array('as' => 'users.register', 'uses' => 'UsersController@getRegister'));
Route::post('register', array('as' => 'users.register', 'uses' => 'UsersController@postRegister'));

Route::any('/api/get_project/{id?}', array('as' => 'api.get_project', 'uses' => 'ApiController@get_project'));
Route::post('/api/update_project', array('as' => 'api.update_project', 'uses' => 'ApiController@update_project'));

Route::any('/api/upload_ufs/{id}', array('as' => 'projects.upload_ufs', 'uses' => 'ApiController@upload_ufs'));

Route::group(array('before' => 'users.login'), function()
{
    Route::get('/', array('as' => 'app.getindex', 'uses' => /*'AppController@getIndex'*/'ProjectsController@getIndex'));
    Route::get('/logout', array('as' => 'users.logout', 'uses' => 'UsersController@getLogout'));
    Route::get('/usersgroup', array('as' => 'users.usersgroup.index', 'uses' => 'UsersController@getUsersgroupIndex'));
    Route::get('/usersgroup/edit/{id?}', array('as' => 'users.usersgroup.edit', 'uses' => 'UsersController@getUsersgroupEdit'));
    Route::get('/usersgroup/delete/{id}', array('as' => 'users.usersgroup.delete', 'uses' => 'UsersController@getUsersgroupDelete'));

    Route::get('/users', array('as' => 'users.users.index', 'uses' => 'UsersController@getUsersIndex'));
    Route::get('/users/edit/{id?}', array('as' => 'users.users.edit', 'uses' => 'UsersController@getUsersEdit'));
    Route::get('/users/delete/{id}', array('as' => 'users.users.delete', 'uses' => 'UsersController@getUsersDelete'));

    Route::get('/projects', array('as' => 'projects.getindex', 'uses' => 'ProjectsController@getIndex'));

    //использование оптимизированных систем
    Route::get('/projects/usesystem/{id?}', array('as' => 'projects.use_system', 'uses' => 'ProjectsController@use_system'));
    Route::post('/projects/usesystem/{id?}', array('as' => 'projects.use_system', 'uses' => 'ProjectsController@get_system_val'));

    Route::get('/projects/edit/{id?}', array('as' => 'projects.getedit', 'uses' => 'ProjectsController@getEdit'));
    Route::post('/projects/edit/{id?}', array('as' => 'projects.postedit', 'uses' => 'ProjectsController@postEdit'));
    Route::get('/projects/delete/{id}', array('as' => 'projects.getdelete', 'uses' => 'ProjectsController@getDelete'));
    Route::get('/projects/setstate/{id}/{state}', array('as' => 'projects.getsetstate', 'uses' => 'ProjectsController@getSetstate'));

    Route::post('/projects/loadtables/{id}/', array('as' => 'projects.loadtables', 'uses' => 'ProjectsController@postLoadTables'));

    Route::post('/projects/slognost/{id}/', array('as' => 'projects.slognost', 'uses' => 'ProjectsController@postSelectSlognost'));
    Route::post('/projects/optimizationparams/{id}/', array('as' => 'projects.optimizationparams', 'uses' => 'ProjectsController@postParamOptiomizations'));
    Route::post('/projects/functiontype/{id}/', array('as' => 'projects.functiontype', 'uses' => 'ProjectsController@postParamFunctiontype'));
    Route::post('/projects/savealgparam/{id}/{itemid}', array('as' => 'projects.savealgparam', 'uses' => 'ProjectsController@postSaveAlgParams'));
    Route::post('/projects/addalg/{id}/', array('as' => 'projects.addalg', 'uses' => 'ProjectsController@postAddAlg'));
    Route::get('/projects/delalg/{id}/{itemid}', array('as' => 'projects.delalg', 'uses' => 'ProjectsController@postDelAlg'));
    Route::get('/projects/resort/{id}/{itemid}/{type}', array('as' => 'projects.resort', 'uses' => 'ProjectsController@postResortAlg'));
    Route::get('/projects/deletetable/{id}/{type}', array('as' => 'projects.deletetable', 'uses' => 'ProjectsController@getDelTables'));



    Route::get('/algorithms', array('as' => 'algorithms.getindex', 'uses' => 'AlgorithmsController@getIndex'));

    Route::get('/algorithms/edit/{id?}', array('as' => 'algorithms.getedit', 'uses' => 'AlgorithmsController@getEdit'));
    Route::post('/algorithms/edit/{id?}', array('as' => 'algorithms.postedit', 'uses' => 'AlgorithmsController@postEdit'));

    Route::post('/algorithms/blockparam', array('as' => 'algorithms.blockparam', 'uses' => 'AlgorithmsController@postBlockparam'));
    Route::post('/algorithms/blockparamlist', array('as' => 'algorithms.blockparamlist', 'uses' => 'AlgorithmsController@postBlockparamlist'));
    Route::post('/algorithms/blockparamdelete', array('as' => 'algorithms.blockparamdelete', 'uses' => 'AlgorithmsController@postBlockparamdelete'));
    Route::post('/algorithms/blockparamlistdelete', array('as' => 'algorithms.blockparamlistdelete', 'uses' => 'AlgorithmsController@postBlockparamlistdelete'));

    Route::post('/algorithms/saveparam', array('as' => 'algorithms.saveparam', 'uses' => 'AlgorithmsController@postSaveParams'));

    Route::get('/algorithms/delete/{id}', array('as' => 'algorithms.getdelete', 'uses' => 'AlgorithmsController@getDelete'));
});