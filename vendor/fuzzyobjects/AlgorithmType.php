<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 03.05.14
 * Time: 20:21
 */

namespace fuzzyobjects;


class ProjectType {
    const RULEGENERATION = 0;
    const APPROXIMATION = 1;
    const CLASSIFICATION = 2;

} 