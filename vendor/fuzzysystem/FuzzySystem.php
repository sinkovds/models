<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 28.04.14
 * Time: 13:29
 */

namespace FuzzySystem;
use Xml;

class FuzzySystem {
    public $type;
    public $variables;
    public $rules;
    public $observations;

    public function read_from_xml($path){
        $file = Xml::build($path);
        $system = Xml::toArray($file);
        //pr($system);
        $this->type = $system['FuzzySystem']['@Type'];

        $this->variables = array();

        foreach($system['FuzzySystem']['Variables']['Variable'] as $variable){
            $var = new Variable();
            $var->name = $variable['@Name'];
            $var->min = doubleval($variable['@Min']);
            $var->max = doubleval($variable['@Max']);
            $var->terms = array();
            foreach($variable['Terms']['Term'] as $term)
            {
                $term_ = new Term();
                $term_->name = $term['@Name'];
                $term_->type = $term['@Type'];
                $term_->params = array();
                foreach($term['Params']['Param'] as $param){
                    $term_->params[$param['@Number']] = doubleval($param['@Value']);
                }
                $var->terms[$term_->name] = $term_;
            }
            $this->variables[$var->name] = $var;
        }
        //pr($this->variables);

        $this->rules = array();

        foreach($system['FuzzySystem']['Rules']['Rule'] as $rule){
            $rule_ = new Rule();
            $rule_->antecedent = array();
            foreach($rule['Antecedent']['Pair'] as $pair){
                $rule_->antecedent[$pair['@Variable']] = $pair['@Term'];
            }
            $rule_->consequent = doubleval($rule['Consequent']['@Value']);
            $this->rules[] = $rule_;
        }

        //pr($this);

        //die;
    }

    public function inference($values){
        $sum1 = 0;
        $sum2 = 0;
        foreach($this->rules as $rule){
            $pr = 1;

            foreach($rule->antecedent as $var_name=>$term_name)
            {
                $pr *= ($this->variables[$var_name]->terms[$term_name]->getValue($values[$var_name]));

            }
            $sum1 += $pr * $rule->consequent;
            $sum2 += $pr;
        }
        if($sum2 == 0)
            return 'Error';

        $result = $sum1/$sum2;

        //die($result);

        return $result;
    }
} 