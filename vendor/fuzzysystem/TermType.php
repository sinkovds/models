<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 28.04.14
 * Time: 13:23
 */

namespace FuzzySystem;


class TermType {
    const TRIANGLE = "Triangle";
    const GAUSS = "Gauss";
    const TRAPEZOID = "Trapezoid";
    const PARABOLIC= "Parabolic";
}