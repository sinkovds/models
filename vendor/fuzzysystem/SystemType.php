<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 28.04.14
 * Time: 16:01
 */

namespace FuzzySystem;


class SystemType {
    const ClassifierPittsburgh = "ClassifierPittsburgh";
    const ClassifierMichigan = "ClassifierMichigan";
    const ApproximatorSingleton = "ApproximatorSingleton";
    const ApproximatorTakagiSugeno = "ApproximatorTakagiSugeno";
    const ApproximatorMamdani = "ApproximatorMamdani";
}