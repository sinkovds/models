<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinkov
 * Date: 28.04.14
 * Time: 13:18
 */

namespace FuzzySystem;


class Term {
    public $name;
    public $type;
    public $params;
    public  function getValue($X){


        switch($this->type){
            case TermType::TRIANGLE:
                if($X == $this->params[0] && $X == $this->params[1])
                    return 1;
                if($X == $this->params[1] && $X == $this->params[2])
                    return 1;

                if($X >= $this->params[0] && $X <= $this->params[1]){
                    return ($X - $this->params[0])/($this->params[1] - $this->params[0]);
                }

                if($X >= $this->params[1] && $X <= $this->params[2]){
                    return ($X - $this->params[2])/($this->params[1] - $this->params[2]);
                }
                return 0;
                break;
            case TermType::GAUSS:
                break;
            case TermType::PARABOLIC:
                break;
            case TermType::TRAPEZOID:
                break;
        }

        return 0;
    }
} 